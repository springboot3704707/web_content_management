package kh.edu.bbu.web_content_management.constants;

import kh.edu.bbu.web_content_management.model.request.ItemKeyValue;

import java.util.ArrayList;
import java.util.List;

public class Constant {
    public static List<ItemKeyValue> getAllStatus(){
        List<ItemKeyValue> itemKeyValues = new ArrayList<>();
        itemKeyValues.add(new ItemKeyValue(1,"ACT","Active"));
        itemKeyValues.add(new ItemKeyValue(2,"DEL","Delete"));
        itemKeyValues.add(new ItemKeyValue(3,"DSL","Disable"));
        return itemKeyValues;
    }
    public static List<ItemKeyValue> getStockType(){
        List<ItemKeyValue> itemKeyValues = new ArrayList<>();
        itemKeyValues.add(new ItemKeyValue(1,"C","Cut Stock"));
        itemKeyValues.add(new ItemKeyValue(2,"P","Put Stock"));
        return itemKeyValues;
    }
    public static List<String> getAllStatusString(){
        List<String> statustList = new ArrayList<>();
        statustList.add("ACT");
        statustList.add("DEL");
        statustList.add("DSL");
        return statustList;
    }
    public static List<String> getAllActive(){
        List<String> statusActiveList = new ArrayList<>();
        statusActiveList.add("ACT");
        return statusActiveList;
    }
}
