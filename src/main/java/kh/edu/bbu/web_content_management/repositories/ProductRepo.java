package kh.edu.bbu.web_content_management.repositories;
import kh.edu.bbu.web_content_management.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductRepo extends JpaRepository<Product, Integer> {
    List<Product> findAllByStatusInOrderByIdDesc(List<String> statusList);
   List<Product> findAllByStatusInOrderByIdAsc(List<String> getAllActive);
}
