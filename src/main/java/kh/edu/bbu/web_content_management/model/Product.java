package kh.edu.bbu.web_content_management.model;

import jakarta.persistence.*;
import kh.edu.bbu.web_content_management.model.request.ItemKeyValue;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "product")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private String code;
    private String barcode;
    @Transient
    private List<ItemKeyValue> statusList;
    @ManyToOne
    private Category category;
    @Transient
    private List<Category> categories;
    private double cost;
    private double price;
    private int qty;
    private String description;
    @Transient
    private List<ItemKeyValue> stock_typeList;
    private String stock_type;
    private String status;
}
