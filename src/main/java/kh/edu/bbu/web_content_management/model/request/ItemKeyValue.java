package kh.edu.bbu.web_content_management.model.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ItemKeyValue {
    private int id;
    private String key;
    private String value;
}
