package kh.edu.bbu.web_content_management.model;


import jakarta.persistence.*;
import kh.edu.bbu.web_content_management.model.request.ItemKeyValue;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Entity
@Table(name = "categories")
@ToString
@Getter
@Setter
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private String nameKh;
    private String status;
    @Transient
    private List<ItemKeyValue> statusList;
}
