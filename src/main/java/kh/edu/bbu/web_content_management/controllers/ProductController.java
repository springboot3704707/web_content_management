package kh.edu.bbu.web_content_management.controllers;

import kh.edu.bbu.web_content_management.constants.Constant;
import kh.edu.bbu.web_content_management.model.Product;
import kh.edu.bbu.web_content_management.repositories.CategoryRepository;
import kh.edu.bbu.web_content_management.repositories.ProductRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.Banner;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/products")
@RequiredArgsConstructor
public class ProductController {
    private final ProductRepo productRepo;
    private final CategoryRepository categoryRepository;
    @GetMapping
    public String index(Model model){
        model.addAttribute("products", productRepo.findAllByStatusInOrderByIdDesc(Constant.getAllStatusString()));
        return "product/index";
    }
    @GetMapping("/create")
    public String create(Model model){
        Product product = new Product();
        product.setCategories(categoryRepository.findAllByStatus("ACT"));
        product.setStatusList(Constant.getAllStatus());
        product.setStock_typeList(Constant.getStockType());
        model.addAttribute("product", product );
        return "product/create";
    }
    @PostMapping("/create")
    public String create(Model model, @ModelAttribute("product") Product req){
        productRepo.save(req);
        return "redirect:/products";
    }
    @GetMapping("/edit/{id}")
    public String edit(Model model, @PathVariable("id") Integer id){
        Product product = productRepo.findById(id).orElse(null);
            if(product !=null){
                product.setCategories(categoryRepository.findAllByStatus("ACT"));
                product.setStock_typeList(Constant.getStockType());
                product.setStatusList(Constant.getAllStatus());
                model.addAttribute("product", product);
                return "product/create";
            }
        return ("redirect:/products");
    }
    @GetMapping("/delete/{id}")
    public String delete(Model model, @PathVariable("id") Integer id){
        Product product = productRepo.findById(id).orElse(null);
        if(product !=null){
            product.setStatus("DEL");
            productRepo.save(product);
        }
        return ("redirect:/products");
    }


}
