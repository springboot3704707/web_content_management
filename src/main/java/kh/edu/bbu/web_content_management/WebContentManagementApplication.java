package kh.edu.bbu.web_content_management;

import kh.edu.bbu.web_content_management.repositories.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebContentManagementApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebContentManagementApplication.class, args);
    }

}
